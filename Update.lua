local autoupdate = true
local downloadmissinglibs = true

local version = 0.06
local scriptname = "Update"
local scripturl = "https://bitbucket.org/Johan95/bol-scripts/raw/master/Update.lua"
local scriptversionurl = "https://bitbucket.org/Johan95/bol-scripts/raw/master/Version/Update.version"



function exist(_file, _l)
	local path
	if _l == 1 then
		path = BOL_PATH .. "\\Scripts\\Common\\" .. _file
	elseif _l then
		path = BOL_PATH .. "\\Scripts\\Common\\" .. _file .. ".lua"
	else
		path = BOL_PATH .. "\\Scripts\\" .. _file .. ".lua"
	end
	local f = io.open(path, "r")
	if f then
		f:close()
		return true
	else
		return false
	end
end

function loadlib(_lib)
	if exist(_lib, true) then
		require(_lib)
	else
		local path = BOL_PATH .. "\\Scripts\\Common\\" .. _lib .. ".lua"
		local URL = {}
		URL.AoESkillshotPosition = "https://raw.github.com/Zikkah/BoL/master/AoE_Skillshot_Position.lua"
		URL.AoE_Skillshot_Position = "https://raw.github.com/Zikkah/BoL/master/AoE_Skillshot_Position.lua"
		URL.Collision = "https://bitbucket.org/Klokje/public-klokjes-bol-scripts/raw/master/Common/Collision.lua"
		URL.DrawDamageLib = "http://pastebin.com/raw.php?i=UfZ0Efax"
		URL.FastCollision = "https://bitbucket.org/boboben1/bol-scripts/raw/master/Misc/FastCollision.lua"
		URL.MapPosition = "http://privatepaste.com/download/b127e3018b"
		URL.Prodiction = "https://bitbucket.org/Klokje/public-klokjes-bol-scripts/raw/master/Common/Prodiction.lua"
		URL.spellList = "https://raw.github.com/SurfaceS/BoL_Studio/master/Scripts/Common/spellList.lua"
		URL.VPrediction = "https://bitbucket.org/honda7/bol/raw/master/Common/VPrediction.lua"
		if URL[_lib] and downloadmissinglibs then
			print("<font color='#EA3B09'>Downloading " .. _lib .. ".lua... Don't press F9 untill it is complete. </font>")
			DownloadFile(URL[_lib], path, function()
				if exist(_lib, true) then
					print("<font color='#00FF00'>Downloaded " .. _lib .. ".lua successfully! Please press F9 twice. </font>")
				end
			end)
		else
			print("<font color='#FF1919'>" .. _lib .. ".lua could not be found. Please download it and place it in \\Scripts\\Common. </font>")
		end
	end
end

function downloadscript()
	DownloadFile(scripturl, BOL_PATH .. "\\Scripts\\" .. scriptname .. ".lua", function()
		print("<font color='#00FF00'>Downloaded latest version of " .. scriptname .. " successfully! Please press F9 twice.")
	end)
end

function versioncheck(_currentversion)
	if _currentversion and version and scriptname then
		local currentversion = tonumber(_currentversion)
		if currentversion > version then
			print("<font color='#EA3B09'>New version available of " .. scriptname .. "! Don't press F9 untill the download is complete. </font>")
			downloadscript()
		elseif currentversion <= version then
			print("<font color='#00FF00'>Version [" .. version .. "] of " .. scriptname .. " Loaded! You have the latest version. </font>")
		else
			print("<font color='#00FF00'>Version [" .. version .. "] of " .. scriptname .. " Loaded! </font>")
		end
	elseif version and scriptname then
		print("<font color='#00FF00'>Version [" .. version .. "] of " .. scriptname .. " Loaded! Auto updater disabled. </font>")
	elseif scriptname then
		print("<font color='#00FF00'>" .. scriptname .. " Loaded!</font>")
	end
end

function updatescript()
	if autoupdate and scriptname and scripturl and scriptversionurl then
		local currentversion
		local path = BOL_PATH .. "\\Scripts\\Common\\" .. scriptname .. ".version"
		os.remove(path)
		DownloadFile(scriptversionurl, path, function()
			if exist(scriptname .. ".version", 1) then
				local f = io.open(path, "r")
				currentversion = f:read'*a'
				f:close()
				os.remove(path)
				versioncheck(currentversion)
			else
				versioncheck(false)
			end
		end)
	else
	downloadscript(false)
	end
end

function OnLoad()

end

function OnTick()

end